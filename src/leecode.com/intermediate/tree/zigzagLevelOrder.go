package main

import(
	"fmt"
)
type TreeNode struct {
    Val int
    Left *TreeNode
    Right *TreeNode
}
func zigzagLevelOrder(root *TreeNode) [][]int {
	res := [][]int{}
	
	if root == nil {
		return res
	}

	stack := []*TreeNode{root}
	flag := 1
	for len(stack) != 0 {
		tmp1 := []*TreeNode{}
		tmp2 := []int{}
		for i:= len(stack)-1; i >= 0; i-- {
			tmpNode := stack[i]
			tmp2 = append(tmp2, tmpNode.Val)
			fmt.Printf("%v,",tmpNode.Val)
			if flag%2 == 0 {
				tmp1 = appendNode(tmp1,tmpNode.Right)
				tmp1 = appendNode(tmp1,tmpNode.Left)
			} else {
				tmp1 = appendNode(tmp1,tmpNode.Left)
				tmp1 = appendNode(tmp1,tmpNode.Right)
			}
		}
		
		fmt.Println("")
		res = append(res,tmp2)
		stack = tmp1
		flag++
	}
	return res
}

func appendNode(stack []*TreeNode, node *TreeNode) []*TreeNode{
	if node != nil {
		stack = append(stack,node)
	}
	return stack
}

func main() {
	var n1,n2,n3,n4,n5,n6,n7 TreeNode
	n1.Val = 1
	n2.Val = 2
	n3.Val = 3
	n4.Val = 4
	n5.Val = 5
	n6.Val = 6
	n7.Val = 7

	n1.Left = &n2
	n1.Right = &n3
	n2.Left = &n4
	n2.Right = &n5
	n3.Left = &n6
	n3.Right = &n7

	zigzagLevelOrder(&n1)
}