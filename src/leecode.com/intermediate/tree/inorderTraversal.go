package main

import (
	"fmt"
)

func inorderTraversal(root *TreeNode) []int {
	stack := []*TreeNode{}
	res := []int{}

	if root == nil {
		return res
	}

	node := root
	for node != nil {
		//fmt.Println("node:", node.Val)
		if node.Left != nil {
			stack = append(stack,node)
			node = node.Left
			continue
		}

		if node.Left == nil {
			res = append(res, node.Val)
			fmt.Println("val:", node.Val)
			if node.Right != nil {
				node = node.Right
			} else {
				length := len(stack)
				if length == 0 {
					node = nil
					continue
				} else {
					node = stack[length - 1]
					node.Left = nil
					stack = stack[:length-1]
				}
			}
		}
	} 
    return res
}