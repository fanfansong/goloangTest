package main

import (
	//"math"
	"fmt"
)
func threeSum0(nums []int) [][]int {
	length := len(nums)
	res := [][]int{}
    for x := 0;x < length-2;x++ {
		for y := x +1; y < length -1; y++ {
			for z := y + 1; z < length;z++ {
				if nums[x] + nums[y] + nums[z] == 0 {
					res = append(res, []int{nums[x],nums[y],nums[z]})
				}
			}
		}
	}
	fmt.Println("len:",len(res))
	return removal(res)
}

func removal(lists [][]int) [][]int{
	length := len(lists)
	if length == 0 {
		return [][]int{}
	} else if length == 1 {
		return [][]int{lists[0]}
	}

	for i := 0; i < len(lists) -1; i++ {
		for j:= i + 1; j < len(lists) ; {
			if isEqual(lists[i],lists[j]) {
				if j == len(lists) -1 {
					lists = lists[:j]
				} else {
					lists = append(lists[:j],lists[j+1:]...)
				}
			} else {
				j++
			}
		}
	}

	return lists
}

//相等返回ture
func isEqual(a []int, b []int) bool {
	if a == nil || b == nil{
		return false
	}
	map1 := make(map[int]int)

	for _,val := range a {
		map1[val]++
	}

	for _,val := range b {
		map1[val]--
	}

	for _,val := range map1 {
		if val != 0 {
			return false
		}
	}

	return true
}

func threeSum2(nums []int) [][]int {
	length := len(nums)
	res := [][]int{}
	mx := make(map[int]bool)
    for x := 0;x < length-2;x++ {
		numX := nums[x]
		if mx[numX] {
			continue
		}
		mx[numX] = true
		my := make(map[int]bool)
		for y := x +1; y < length -1; y++ {
			numY := nums[y]
			if my[numY] {
				continue
			}
			my[numY] = true
			mz := make(map[int]bool)
			for z := y + 1; z < length;z++ {
				numZ := nums[z]
				if mz[numZ] {
					continue
				}
				mz[numZ] = true
				if numX + numY + numZ == 0 {
					res = append(res, []int{numX,numY,numZ})
				}
			}
		}
	}
	//fmt.Println("len:",len(res))
	return removal(res)
}


func threeSum(nums []int) [][]int {
	if len(nums) < 3 {
		return [][]int{}
	}

	uniqueList,repeatMap,uniqueMap := removalList(nums)

	length := len(uniqueList)
	res := [][]int{}
	if repeatMap[0] >= 2 {
		res = append(res,[]int{0,0,0})
	}

    for x := 0;x < length-1;x++ {
		vx := uniqueList[x]
		for y := x +1; y < length; y++ {
			vy := uniqueList[y]
			vz := -(vx+vy)
			//fmt.Printf("x:%v,y:%v\n", x,y)
			//fmt.Printf("vx:%v,vy:%v,vz:%v\n", vx,vy,vz)
			if vx == vz || vy == vz {
				if _,ok:=repeatMap[vz]; ok {
					//fmt.Printf("repeatMap[vz]:%v\n",repeatMap[vz])
					res = append(res,[]int{vx,vy,vz})
				}
			} else {
				if index,ok := uniqueMap[vz];ok {
					if index > y {
						//fmt.Printf("uniqueMap[%v]:%v\n",index,uniqueMap[vz])
						res = append(res,[]int{vx,vy,vz})
					}
				}
			}
		}
	}
	return res
}

func removalList(nums []int) ([]int,map[int]int,map[int]int) {
	m := make(map[int]int)
	uniqueList := []int{}
	repeatMap := make(map[int]int)

	index := 0
	for _,val := range nums {
		_,ok := m[val]
		if ok {
			repeatMap[val]++
		} else {
			uniqueList = append(uniqueList,val)
			m[val] = index
			index++
		}
	}
	return uniqueList,repeatMap,m
}


func main1(){
	//list := []int{-4,-8,7,13,10,1,-14,-13,0,8,6,-13,-5,-4,-12,2,-11,7,-5,0,-9,-14,-8,-9,2,-7,-13,-3,13,9,-14,-6,8,1,14,-5,-13,8,-10,-5,1,11,-11,3,14,-8,-10,-12,6,-8,-5,13,-15,2,11,-5,10,6,-1,1,0,0,2,-7,8,-6,3,3,-13,8,5,-5,-3,9,5,-4,-14,11,-8,7,10,-6,-3,11,12,-14,-9,-1,7,5,-15,14,12,-5,-8,-2,4,2,-14,-2,-12,6,8,0,0,-2,3,-7,-14,2,7,12,12,12,0,9,13,-2,-15,-3,10,-14,-4,7,-12,3,-10}
	//list := []int{-1,0,1,-1,2,-4}
	list := []int{1,1,-2}
	res :=threeSum(list)
	for _,list := range res {
		for _,val := range list {
			fmt.Printf("%v,",val)
		}
		fmt.Println("")
	}
}