package main 

import (
	"fmt"
)

func longestPalindrome(s string) string {
	length := len(s)
	if length == 0 || length == 1 {
		return s
	}

	var maxStr string
	for i:= 0; i < length -1; i++ {
		for j:= length -1; i < j;j-- {
			if s[i] == s[j] {
				move,ok := check(s[i:j+1])
				if ok  && move > len(maxStr){
					maxStr = s[i:j+1]
					//i = i+move
					break
				} //else {
				//	j = j-move
				//}
			}
		}
	}

	if len(maxStr) == 0 {
		return s[:1]
	}
    return maxStr
}

func check(s string) (int,bool) {
	length := len(s)
	start := 0;
	end := length-1
	for start < end {
		if s[start] != s[end] {
			return start + 1, false
		}
		start++
		end--
	}
	return length,true
}


func main6() {
	res1,ok := check("aaabaaaa")
	fmt.Printf("[func check]ok:%v, res:%v\n", ok, res1)

	res2 := longestPalindrome("aaabaaaa")
	fmt.Printf("[func longestPalindrome] res:%v\n", res2)
}