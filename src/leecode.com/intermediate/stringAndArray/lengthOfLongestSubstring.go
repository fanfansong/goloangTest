package main

import (
	"fmt"
	"math"
)


func lengthOfLongestSubstring(s string) int {
	length := len(s)
	if length == 0 {
		return 0
	} 
	if length == 1 {
		return 1
	}

	indexMap := make(map[int]bool)
	max := math.MinInt32
	for i:= 0; i < length; i++ {
		if indexMap[i] {
			continue
		}
		runeMap := make(map[byte]bool)
		count := 0
		for j:= i; j < length; j++ {
			if !runeMap[s[j]] {
				runeMap[s[j]] = true
				count++
			} else {
				indexMap[j] = true
				if count > max {
					max = count
				}
				runeMap = make(map[byte]bool)
				runeMap[s[j]] = true
				count = 1
			}
		}
		if count > max {
			max = count
		}
	}

	return max
}

//别人的代码值用了一次循环
func lengthOfLongestSubstring2(s string) int {
    lastOccurred := make(map[byte]int)
	start := 0
	maxLength := 0
	for i, ch := range []byte(s) {
		fmt.Printf("start:%v, maxLength:%v\n", start,maxLength)
		lastI, ok := lastOccurred[ch]
		if ok && lastI >= start {
			fmt.Printf("lastI:%v", lastI)
			start = lastI + 1
		}
		if i-start +1 > maxLength {
			maxLength = i-start+1
		}
		lastOccurred[ch] = i
	}
	return maxLength
}

func main5() {
	res := lengthOfLongestSubstring2("abcdefagb")
	fmt.Println("res:", res)
}