package main

import (
	"fmt"
	"math"
)

func increasingTriplet(nums []int) bool {
	length := len(nums)
	if length < 3 {
		return false
	}

	min := nums[0]
	middle := math.MaxInt32
	mmin := math.MaxInt32
	for i:=1; i < length; i++ {
		fmt.Printf("nums[%v]:%v, min:%v, midde:%v, mmin:%v\n", i, nums[i], min,middle,mmin)
		if nums[i] > middle {
			return true
		}

		if nums[i]< middle && nums[i] > min {
			middle = nums[i]
			continue
		}

		if nums[i] < min && nums[i] < mmin {
			mmin = nums[i]
			continue
		}

		if nums[i] <= min && nums[i] > mmin {
			min = mmin
			middle = nums[i]
			mmin = math.MaxInt32
			continue
		}
	}
	return false
}


func increasingTriplet2(nums []int) bool {
	if len(nums) < 3 {
		return false
	}

	c1 := 1<<32 - 1
	c2 := 1<<32 - 1

	for _, val := range nums {
		fmt.Printf("val:%v, c1:%v, c2:%v\n", val, c1,c2)
		if val <= c1 {
			c1 = val
		} else if val <= c2 {
			c2 = val
		} else {
			return true
		}
	}
	return false
}

func main() {
	//list := []int{1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2}
	list := []int{1,8,5,4}
	res := increasingTriplet2(list)
	fmt.Println("increasingTriplet:" ,res)
}