package main 

import (
	"fmt"
)

func groupAnagrams(strs []string) [][]string {
	res := [][]string{}
	maxIndex := 0
	strMap :=  make(map[string]int)

	for _,str := range strs {

		sortString := sortStr(str)
		if index,ok :=strMap[sortString];ok {
			//var flag bool
			//for i := 0; i < len(res[index]); i++ {
			//	if res[index][i] == str {
			//		flag = true
			//		break
			//	}
			//}
			//if !flag {
			//	res[index] = append(res[index],str)
			//}
			res[index] = append(res[index],str)
		} else {
			res = append(res,[]string{str})
			strMap[sortString] = maxIndex
			maxIndex++
		}

	}

	return res
}


func sortStr(s string) string{
	if len(s) == 1|| len(s) == 0 {
		return s
	}

	res := []byte(s)
	for i := 0; i < len(res) -1 ; i++ {
		for j:= i+1; j < len(res); j++ {
			if res[i] > res[j] {
				res[i],res[j] = res[j],res[i]
			}
		}
	}

	return string(res)
}

func main4() {
	//res := sortStr("acb")
	//fmt.Println("res:",res)

	//list := []string{"eat", "tea", "tan", "ate", "nat", "bat"}
	list := []string{"",""}
	res2:= groupAnagrams(list)
	for _,list := range res2 {
		for _,s := range list {
			fmt.Printf("%v,", s)
		}
		fmt.Println("")
	}
}