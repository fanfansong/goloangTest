package main 

import (
	//"fmt"
)

func setZeroes(matrix [][]int)  {
	xlen := len(matrix)
	if xlen == 0 {
		return
	}

	var xFlag bool
	var yFlag bool
	ylen := len(matrix[0])

	for y := 0; y < ylen ; y++ {
		if matrix[0][y] == 0 {
			xFlag = true
			break
		}
	}

	for x := 0; x < xlen ; x++ {
		if matrix[x][0] == 0 {
			yFlag = true
			break
		}
	}

	if xlen == 1 && xFlag{
		for y := 0; y < ylen; y++ {
			matrix[0][y] = 0
		}
		return
	}

	if ylen == 1 && yFlag{
		for x := 0; x < xlen; x++ {
			matrix[x][0] = 0
		}
		return
	}

	for x := 0; x < xlen; x++ {
		for y := 0; y < ylen; y++ {
			if matrix[x][y] == 0 {
				//fmt.Printf("x:%v,y:%v\n",x,y)
				matrix[x][0] = 0
				matrix[0][y] = 0
			}
		}
	}

	for y := 1; y < ylen; y++ {
		//fmt.Printf("matrix[0][%v]:%v\n",y,matrix[0][y])
		if matrix[0][y] == 0 {
			for x :=0; x < xlen;x++ {
				matrix[x][y] = 0
			}
		}

		if xFlag {
			matrix[0][y] = 0
		}
	}

	for x := 1; x < xlen; x++ {
		//fmt.Printf("matrix[%v][0]:%v\n",x,matrix[x][0])
		if matrix[x][0] == 0 {
			for y :=0; y < ylen;y++ {
				matrix[x][y] = 0
			}
		}

		if yFlag {
			matrix[x][0] = 0
		}
	}

}

func main2() {
	l1 := []int{0,1,2,0}
	l2 := []int{3,4,5,2}
	l3 := []int{1,3,1,5}

	setZeroes([][]int{l1,l2,l3})
}