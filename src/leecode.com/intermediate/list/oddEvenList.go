package main

func oddEvenList(head *ListNode) *ListNode {
    if head == nil || head.Next == nil{
		return head
	}

	node1 := head
	node2 := node1.Next
    secondHead := head.Next
	for node1.Next != nil || node2.Next != nil {
        //fmt.Printf("start node1:%v,node2:%v\n", node1.Val,node2.Val)
		node1.Next = node2.Next
        if node2.Next != nil {
            node1 = node2.Next
        }
		node2.Next = node1.Next
        if node1.Next == nil {
            break
        }
        node2 = node1.Next
	}

	node1.Next = secondHead
	return head
}

func main() {

}