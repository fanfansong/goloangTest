package main

type ListNode struct {
    Val int
    Next *ListNode
}

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	var res *ListNode
	var last *ListNode
	up := 0
	for l1 != nil || l2 != nil {
		val1,val2 := 0,0
		if l1 != nil {
			val1 = l1.Val
			l1 = l1.Next
		} 

		if l2 != nil {
			val2 = l2.Val
			l2 = l2.Next
		}

		val := val1+val2 + up
		var node ListNode
		node.Val = val%10
		if last == nil {
			res = &node
			last = &node
		} else {
			last.Next = &node
			last = &node
		}

		if val/10 > 0 {
			up = 1
		} else {
			up = 0
		}
	}
	if up != 0 {
		var node ListNode
		node.Val = up
		last.Next = &node
	}

	return res
}