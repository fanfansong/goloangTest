
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        if(headA==headB) {
            return headA
        }

        ListNode *node = headA;
        int countA = 0;
        int countB = 0
        while(node != NULL) {
            countA++;
            node = node->next
        }
        node = headB;
        while(node != NULL) {
            countB++;
            node = node->next;
        }
        

        ListNode *nodeA = headA;
        ListNode *nodeB = headB;
        if(countA > countB){
            int sub = countA - countB;
            for(int i = 1; i <= sub;i++) {
                nodeA = nodeA->next;
            }
        } else {
            int sub = countB - countA;
            for(int i = 1; i <= sub;i++) {
                nodeB = nodeB->next;
            }
        }

        for(nodeA->next == NULL || nodeB->next == NULL) {
            if(nodeA->next == nodeB->next) {
                return true
            }
        }
        return false
    }
};