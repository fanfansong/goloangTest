package main

import (
	"fmt"
)

func main() {
	//list := []int{0,1,2,3,4,5}
	//rotate2(list, 3)
	//fmt.Print(list)

	
	//flag1 := containsDuplicate2([]int{0,1,2,3,4,5})
	//flag2 := containsDuplicate2([]int{0,1,2,3,4,1})
	//fmt.Printf("flag1:%v, flag2:%v", flag1, flag2)

	//n := singleNumber([]int{1,2,2})
	//fmt.Print(n)

	list := intersect([]int{1,2}, []int{2,1})
	fmt.Println(list)

	list = plusOne([]int{0})
	fmt.Println(list)

	list = []int{0,1,4,0,5,0,1}
	moveZeroes(list)
	fmt.Println(list)

	board := [][]string {
		{"5","3",".",".","7",".",".",".",".",},
		{"6",".",".","1","9","5",".",".",".",},
		{".","9","8",".",".",".",".","6",".",},
		{"8",".",".",".","6",".",".",".","3",},
		{"4",".",".","8",".","3",".",".","1",},
		{"7",".",".",".","2",".",".",".","6",},
		{".","6",".",".",".",".","2","8",".",},
		{".",".",".","4","1","9",".",".","5",},
		{".",".",".",".","8",".",".","7","9",},
	}
	fmt.Println(isValidSudoku2(board))

	matrix := [][]int{
		{1,2,3,},
		{4,5,6,},
		{7,8,9,},
	}
	rotate3(matrix)
	for _,list := range matrix {
		for _, val := range list {
			fmt.Printf("%d,",val)
		}
		fmt.Println("")
	}
}

// 0,1,2,3,4,5
// 4,5,0,1,2,3    k=2,len=6
func rotate(nums []int, k int)  {
	length := len(nums)
	if k >= length {
		k = k%length
	}
	tmp := make([]int, k,k)
	for i,j := length - 1,k-1; i >= 0 ; i--{
		if j >= 0 {
			tmp [j] = nums[i]
			j--
		}

		if i - k >= 0 {
			nums[i] = nums[i-k]
		} else {
			nums[i] = tmp[i]
		}
	}
}

func rotate2(nums []int, k int) {
	length := len(nums)
	k = k%length
	tmp := append([]int{}, nums[length-k:]...)
	for i := length-1; i >= k; i-- {
		nums[i] = nums[i-k]
	}

	for i,val := range tmp {
		nums[i] = val
	}
}

func containsDuplicate(nums []int) bool {
	length := len(nums)
    for i := 0; i < length; i++ {
		if i + 1 >= length {
			return false
		}
		for j := i+1; j < length; j++  {
			if nums[i] == nums[j] {
				return true
			}
		}
	}
	return false
}

func containsDuplicate2(nums []int) bool {
	m := make(map[int]bool)
	for _,n := range nums {
		if m[n] {
			return true
		} else {
			m[n] = true
		}
	}
	return false
}

func singleNumber(nums []int) int {
	m := make(map[int]int)
	for _,n := range nums {
		m[n]++
	}

	for n,count := range m {
		if count == 1 {
			return n
		}
	}

	return 0
}

func intersect(nums1 []int, nums2 []int) []int {
	if len(nums1) > len(nums2) {
		nums1,nums2 = nums2,nums1
	}

	basicMap := make(map[int]int)
	resMap := make(map[int]int)
	for _,val := range nums1 {
		basicMap[val]++
	}

	list := []int{}
	for _,val := range nums2 {
		if count,ok := basicMap[val]; ok {
			if resMap[val] < count {
				resMap[val]++
				list = append(list,val)
			}
		}
	}
	return list
 }


 func plusOne(digits []int) []int {
	length := len(digits)
	action := false
	
	result := make([]int, length,length)
	for i := length-1; i >=0 ; i-- {
		if !action {
			res := digits[i] + 1
			if res >9 {
				result[i] = 0
				if i == 0 {
					result = append([]int{1},result...)
				}
			} else {
				result[i] = res
				action = true
			}
		} else {
			result[i] = digits[i]
		}
	}
	return result
	
 }

 //{0,1,4,0,5,0,1}
 func moveZeroes(nums []int)  {
	index := 0
	for i := range nums {
		if nums[i] != 0 {
			nums[index] = nums[i]
			index++
		}
	}

	fmt.Println(index)
	for i := index; i < len(nums); i++{
		nums[i] = 0
	}
}

func twoSum(nums []int, target int) []int {
    for i,val1 := range nums {
		for j,val2 := range nums[i+1:] {
			if (val1 + val2) == target {
				return []int{i, i+j+1}
			}
		}
	}
	return []int{}
}

func isValidSudoku(board [][]byte) bool {
	for i := 0; i < 9; i++ {
		m := make(map[byte]bool)
		for j := 0; j <9; j++ {
			val := board[i][j]
			if repeat(m,val) {
				return false
			}
		}
	}

	for i := 0; i < 9; i++ {
		m := make(map[byte]bool)
		for j := 0; j <9; j++ {
			val := board[j][i]
			if repeat(m,val) {
				return false
			}
		}
	}

	for i1 := 0; i1 < 3; i1++ {
		for j1 := 0; j1 < 3; j1++ {
			m := make(map[byte]bool)
			for i2 := 0; i2 < 3; i2++ {
				for j2 := 0; j2 < 3; j2++ {
					val := board[i1*3 + i2][j1*3 + j2]
					if repeat(m,val) {
						return false
					}
				}
			}
		}
	}
    return true
}

func repeat(m map[byte]bool, val byte) bool {
	if val == byte('.') {
		return false
	}
	
	if !m[val]{
		m[val] = true
	} else {
		return true
	}

	return false
}

func isValidSudoku2(board [][]string) bool {
	for i := 0; i < 9; i++ {
		m := make(map[string]bool)
		for j := 0; j <9; j++ {
			val := board[i][j]
			if repeat2(m,val) {
				fmt.Printf("111: x:%d, y:%d\n", i, j)
				return false
			}
		}
	}

	for i := 0; i < 9; i++ {
		m := make(map[string]bool)
		for j := 0; j <9; j++ {
			val := board[j][i]
			if repeat2(m,val) {
				fmt.Printf("222: x:%d, y:%d\n", j, i)
				return false
			}
		}
	}

	for i1 := 0; i1 < 3; i1++ {
		for j1 := 0; j1 < 3; j1++ {
			m := make(map[string]bool)
			for i2 := 0; i2 < 3; i2++ {
				for j2 := 0; j2 < 3; j2++ {
					val := board[i1*3 + i2][j1*3 + j2]
					if repeat2(m,val) {
						fmt.Println("333333333333")
						return false
					}
				}
			}
		}
	}
    return true
}

func repeat2(m map[string]bool, val string) bool {
	if !m[val]{
		m[val] = true
	} else {
		return true
	}

	return false
}

func rotate3(matrix [][]int)  {
	recursionRoate(matrix, 0, len(matrix)-1)
    return
}

func recursionRoate(matrix [][]int, begin,end int)  {
	if(begin >= end) {
		return
	}

	step := end-begin
	tmp := append([]int{}, matrix[begin][begin: end]...)

	for i := 0; i < step; i++ {
		tmp[i],matrix[begin + i][end] =matrix[begin + i][end], tmp [i]
	}

	for i := 0; i < step; i++ {
		tmp[i],matrix[end][end-i] =matrix[end][end-i], tmp [i]
	}

	for i := 0; i < step; i++ {
		tmp[i],matrix[end-i][begin] =matrix[end-i][begin], tmp [i]
	}

	for i := 0; i < step; i++ {
		tmp[i],matrix[begin][begin+i] =matrix[begin][begin+i], tmp [i]
	}

	recursionRoate(matrix, begin+1, end-1)
    return
}