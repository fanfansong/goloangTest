package main

import "fmt"

type ListNode struct {
	Val int
	Next *ListNode
}

func removeNthFromEnd(head *ListNode, n int) *ListNode {
	length := 1
	for node := head; node.Next != nil; node = node.Next {
		length++
	}

	if n == length {
		return head.Next
	}

	if n == 0 || n > length{
		return head
	}

	node := head
	for i := 1; i < length - n ; i++ {
		node = node.Next
	}

	node.Next = node.Next.Next
	return head
}


func reverseList(head *ListNode) *ListNode {
	if head == nil {
		return nil
	}

	var pre *ListNode = nil
	//var next *ListNode = head.Next
	var node *ListNode = head

	for node != nil {
		//next = node.Next
		//node.Next = pre
		//pre = node
		//node = next
		node, node.Next,pre = node.Next, pre, node

	}

	return pre
}

func mergeTwoLists(l1 *ListNode, l2 *ListNode) *ListNode {
    if l1 == nil {
		return l2
	}

	if l2 == nil {
		return l1
	}

	var node1 *ListNode
	var node2 *ListNode
	var start *ListNode

	if l1.Val <= l2.Val {
		node1,start = l1,l1
		node2 = l2
	} else {
		node1,start = l2,l2
		node2 = l1
	}

	for node1 != nil && node2 != nil{
		if node1.Next == nil {
			node1.Next,node1 = node2, node1.Next
		} else if node1.Next.Val <= node2.Val {
			node1 = node1.Next
			continue
		} else if node1.Next.Val > node2.Val {
			node1,node2,node1.Next = node2, node1.Next,node2
		}
	}

	return start
}


func isPalindrome(head *ListNode) bool {
	if head == nil || head.Next == nil{
		return true
	}

	length := 1
	for node := head; node.Next != nil; node = node.Next {
		length++
	}

	t1 := length % 2
	t2 := length / 2
	i,j := 0,0
	list := make([]int, t2, t2)
	for node := head; node != nil; node = node.Next {
		if j < t2 {
			//fmt.Println(i)
			list[i] = node.Val
			i++
			j++
		} else if j >= t2 {
			if j == t2 {
				i--
				j++
				if t1 != 0 {
					continue
				}
			}
			//fmt.Println(i)
			if list[i] != node.Val {
				return false
			} else {
				i--
			}
		}
	}
	return true
}

func main() {
	var node1 ListNode;
	var node2 ListNode;
	var node3 ListNode;
	node1.Val = 1;
	node2.Val = 0;
	node3.Val = 0;
	node1.Next = &node2
	node2.Next = &node3

	res := isPalindrome(&node1)
	fmt.Println(res)
}