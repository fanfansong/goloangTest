package main

import "fmt"

func merge(nums1 []int, m int, nums2 []int, n int)  {
	if n == 0 {
		return
	}

	i := m -1
	j := n -1

	for index := m+n-1; index >= 0 ; index-- {
		fmt.Printf("i:%v,j:%v,index:%v\n", i,j,index)

		if i < 0 {
			nums1[index] = nums2[j]
			j--
			continue
		}

		if j < 0 {
			nums1[index] = nums1[i]
			i--
			continue
		}

		if nums1[i] >= nums2[j] {
			nums1[index] = nums1[i]
			i--
		} else {
			nums1[index] = nums2[j]
			j--
		}
	}
}


func main() {
	l1 := []int{1,2,3,0,0,0}
	l2 := []int{4,5,6}

	merge(l1,3,l2,3)
	fmt.Print(l1)
}