package main

import (
	"strings"
	"fmt"
)

func main() {
	res := reverse(1534236469)
	//res := power(10, 2)
	fmt.Println(res)
	//fmt.Println(1<<31 -1)


	res1 := isAnagram("abc", "bca")
	fmt.Println(res1)

	res2 := isPalindrome("0P")
	fmt.Println(res2)

	res3 := myAtoi("   +0 123")
	fmt.Println(res3)

	res4 := strStr("hello", "ll")
	fmt.Println("strStr:",res4)

	res5 := countAndSay(4)
	fmt.Println("countAndSay:",res5)

	param := []string{"flower","flow","flight"}
	res6 := longestCommonPrefix(param)
	fmt.Println("longestCommonPrefix:",res6)
}

func reverse(x int) int {
	flag := false
    if x < 0 {
		flag = true
		x = -x
	}


	list := []int{}
	start := false
	for ;x != 0; x = x/10 {
		tmp := x % 10
		if start {
			list = append(list, x %10)
		} else {
			if tmp != 0 {
				start = true
				list = append(list, x %10)
			}
		}
	}

	//fmt.Println(list)
	var max int = (1<<31) -1 
	var result int = 0
	length := len(list)-1
	for i := length; i >= 0; i-- {
		if i > 9 {
			return 0
		} else if i == 9 && list[i] >2 {
			return 0
		}

		tmp := list[i] * power(10, length - i)
		if result >= max - tmp {
			return 0
		}

		result += tmp
	}

	if flag {
		return -result
	}
	return result
}


func reverse2(x int) int {
	flag := false
    if x < 0 {
		flag = true
		x = -x
	}


	list := []int{}
	start := false
	for ;x != 0; x = x/10 {
		tmp := x % 10
		if start {
			list = append(list, x %10)
		} else {
			if tmp != 0 {
				start = true
				list = append(list, x %10)
			}
		}
	}

	fmt.Println(list)
	var max uint32 = 1<<31
	var result uint32 = 0
	for i := len(list)-1; i >= 0; i-- {
		tmp := list[i] * power(10, len(list)-1-i)
		fmt.Printf("list[%v]:%v\n", i, tmp)
		result += uint32(list[i] * power(10, len(list)-1-i))

		if result >= max {
			return 0
		}
	}

	if flag {
		tmp := int(result)
		return -tmp
	}
	return int(result)
}

func power(x int,n int) (result int) {
	if n == 0 {
		result = 1
		return
	}
	result = x

	for i := 2; i <= n ; i++ {
		result *= x
	}

	return
}

func firstUniqChar(s string) int {
	strBuf := []byte(s)
	//buf := make([]byte, 26, 26)
	strMap := make(map[byte]int)
	
	//var val byte = 'a'
	//for i := 0; i < 26; i++ {
	//	buf[i] = val
	//	val++
	//}

	for _,b := range strBuf {
		strMap[b]++
	}

	for i,b := range strBuf {
		if strMap[b] == 1 {
			return i
		}
	}

	return -1
}


func isAnagram(s string, t string) bool {
	if len(s) != len(t) {
		return false
	}

	sMap := make(map[byte]int)
	tMap := make(map[byte]int)

	for i := 0; i < len(s); i++ {
		sMap[s[i]]++
		tMap[t[i]]++
	}

	for key,vale:= range sMap {
		if tMap[key] != vale {
			return false
		}
	}

	return true
}

func isPalindrome(s string) bool {
	s = strings.ToLower(s)
    for i,j:= 0,len(s)-1; i < j; {
		x := s[i]
		y := s[j]

		if !isNum(x) && !isLetter(x) {
			i++
			continue
		}

		if !isNum(y) && !isLetter(y) {
			j--
			continue
		}

		//fmt.Printf("%c,%c\n", x, y)
		//var sub int = int(x) - int(y)
		if x != y {
			return false
		}
		i++
		j--
	}

	return true
}

func isNum(n byte) bool {
	if n >= '0' && n <= '9' {
		return true
	}
	return false
}

func isLetter(n byte) bool {
	if n >= 'A' && n <='Z' {
		return true
	}

	if n >= 'a' && n <= 'z' {
		return true
	}

	return false
}

func myAtoi(str string) int {
	var result uint64 = 0
	flag := false
	start := false

	const MAX_INT = int(^uint32(0) >> 1)
	const MIN_INT = ^MAX_INT
	for i := range str {
		n := str[i]
		if n == ' ' && !start{
			continue
		}

		if n == '+' && !start {
			start = true
			continue
		}

		if n == '-' && !start {
			flag = true
			start = true
			continue
		}

		if !isNum(n) {
			break
		} else if !start {
			start =  true
		}

		result *= 10
		result += uint64(n) - uint64('0')
		if result > uint64(MAX_INT) {
			if flag {
				return MIN_INT
			} else {
				return MAX_INT
			}
		}

	}

	if flag {
		return -int(result)
	}
	return int(result)
}

func strStr(haystack string, needle string) int {
	needleLength := len(needle)
	haystackLength := len(haystack)
    if needleLength == 0 {
		return 0
	}

	if haystackLength == 0 {
		return -1
	}

	for i := range haystack {
		if haystack[i] == needle[0] {
			if i + needleLength > haystackLength {
				return -1
			}

			for j := range needle {
				if needle[j] !=  haystack[i+j] {
					break
				} else if j == needleLength-1 {
					return i
				}
			}
		}
	}
	return -1
}

func countAndSay(n int) string {
	start := []byte{'1'}
	result := count1(n, start)
	return string(result)
}

func count1(n int, list []byte) []byte {
	if n == 1 {
		return list
	}

	result := []byte{}	
	count := 0
	basic := list[0]
	for i := range list {
		if list[i] == basic {
			count++
		} else {
			result = append(result, byte('0'+count))
			result = append(result, basic)
			count = 1
			basic = list[i]
		}
	}
	result = append(result, byte('0'+count))
	result = append(result, basic)

	return count1(n -1,result)
}

func longestCommonPrefix(strs []string) string {
	if len(strs) == 0 {
		return ""
	}

	if len(strs[0]) == 0 {
		return ""
	}

	result := []byte{}
	for i := range strs[0] {
		basic := strs[0][i]
		for _,str := range strs {
			if i >= len(str) || str[i] != basic{
				return string(result)
			}
		}
		result = append(result, basic)
	}
	return string(result)
}