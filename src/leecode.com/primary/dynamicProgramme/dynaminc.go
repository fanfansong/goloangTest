package main

import (
	"math"
	"fmt"
)

func climbStairs(n int) int {
	if n == 0 {
		return 0
	}

	if n == 1 {
		return 1
	}

	val1 := 1
	val2 := 1

	result := 0
	for i := 2; i <= n ; i++ {
		//fmt.Printf("i:%v, val1:%v, val2:%v\n",i,val1,val2)
		result = val1 + val2
		val1,val2 = val2,result
	}

	return result
}


func maxProfit(prices []int) int {
	length := len(prices) 
	fmt.Println("prices len:",length)
	if length == 0 || length == 1 {
		return 0
	}

	max := math.MinInt32
	min := math.MaxInt32
	var maxIndex,minIndex int

	for i,val := range prices {
		fmt.Printf("prices[%v]=%v\n",i,val)
		if val > max {
			max = val
			maxIndex = i
		}
		if val < min{
			min = val
			minIndex = i
		}
	}

	fmt.Printf("maxIndex:%v, minIndex:%v\n",maxIndex,minIndex)
	fmt.Printf("max:%v, min:%v\n",max,min)
	if maxIndex >= minIndex {
		return max - min 
	} else {
		if length == 2  {
			return 0
		}
		res1,res2 := 0,0
		res1 = maxProfit(prices[:maxIndex+1])
		res2 = maxProfit(prices[maxIndex+1:])
		fmt.Printf("res1:%v,res2:%v\n",res1,res2)
		if res1 > res2 {
			return res1
		} else {
			return res2
		}
	}
}

//参考答案，直接比较差值，很快就出来了
func maxProfit2(prices []int) int {
    if len(prices) == 0 {
        return 0
    }
    
	min := prices[0]
	res := 0
	for _, val := range prices {
		if val < min {
			min = val
		}
		diff := val - min
		if diff > res {
			res = diff
		}
	}

	return res
}


func maxSubArray(nums []int) int {
	res := math.MinInt32
	for i,val := range nums {
		sum := val
		if sum > res {
			res = sum
		}
		if i == len(nums) - 1 {
			continue
		}

		for _,val2 := range nums[i+1:] {
			sum += val2
			if sum > res {
				res = sum
			}
		}
	}
	return res
}

func maxSubArray2(nums []int) int {
    
    maxSum, thisSum := math.MinInt64, 0
    for i := 0; i < len(nums); i++ {
        thisSum += nums[i]
        
        if thisSum > maxSum {
            maxSum = thisSum
        }
        
        if thisSum < 0 {
            thisSum = 0
        }
    }
    return maxSum
}


func rob(nums []int) int {
	length := len(nums)

	if length == 0 {
		return 0
	}

	if length == 1 {
		return nums[0]
	}

	a := nums[0]
	var b int
	if nums[0] > nums[1] {
		b = nums[0]
	} else {
		b = nums[1]
	}

	for i := 2; i < length; i++ {
		fmt.Printf("a:%v,b:%v,nums[%v]:%v\n",a,b,i,nums[i])
		tmp := b
		val := nums[i] + a
		if val > b {
			b = val
		}
		a = tmp
	}
	return b
}

func main() {
	//res := climbStairs(44)
	//fmt.Print("climbStairs:", res)

	//res1:=maxProfit([]int{7,1,5,3,6,4})
	//fmt.Print("maxProfit:",res1)

	//res2:=maxSubArray2([]int{-1,-2,-3})
	//fmt.Println("maxSubArray2:",res2)

	res3 := rob([]int{0,1,7,5})
	fmt.Println("rob:",res3)
}