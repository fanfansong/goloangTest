package main

import (
	"math"
	"fmt"
)

type TreeNode struct {
	Val int
	Left *TreeNode
	Right *TreeNode
}

func maxDepth(root *TreeNode) int {
    return depth(root, 0)
}

func depth(node *TreeNode, num int) int {
	if node == nil {
		return num
	}

	num++
	rDepth := depth(node.Right, num)
	lDepth := depth(node.Left, num)

	if rDepth >= lDepth {
		return rDepth
	} else {
		return lDepth
	}
}


func maxDepth2(root *TreeNode) int {
    if root == nil {
        return 0
    }
	
    lDepth := maxDepth(root.Left)
    rDepth := maxDepth(root.Right)
    
    if lDepth > rDepth {
        return lDepth + 1
    } else {
        return rDepth + 1
    }
}

func isValidBST(root *TreeNode) bool {
    if root == nil {
		return true
	}

	res := []int{math.MinInt32,0}
	flag := vaildBst(root, res)
	return flag
}

func vaildBst(node *TreeNode,res []int) bool {
	if node.Left != nil {
		flag := vaildBst(node.Left,res)
		if !flag {
			return false 
		}
	}

	if res[0] >= node.Val && res[1] != 0{
		return false
	}

	res[0] = node.Val
	if res[1] == 0 {
		res[1] = 1
	}

	if node.Right != nil {
		flag := vaildBst(node.Right,res)
		if !flag {
			return false
		}
	}
	return true
}

func isSymmetric(root *TreeNode) bool {
	list := []*TreeNode{root}

	flag := 0
	for flag == 0 {
		tmp := []*TreeNode{}
		fmt.Println("tmp1 len:",len(tmp))
		for i,node := range list {
			fmt.Printf("list[%v]:",i)
			if node == nil {
				fmt.Println("")
				continue
			}
			fmt.Println(node.Val)
			tmp = append(tmp, node.Left)
			tmp = append(tmp, node.Right)
		}
		fmt.Println("tmp2 len:",len(tmp))
		flag = isPalindrome(tmp)
		fmt.Println("flag:",flag)
		if flag == -1 {
			return false
		} else if flag == 1 {
			return true
		} 
		list = tmp
	}

    return true
}

//返回值
//0表示继续，1表示正确并停止，-1表示错误并停止
func isPalindrome(list []*TreeNode) int {
	if len(list) == 0 {
		return 1
	}
	nilNum := 0
	for i,j := 0, len(list)-1; i < j;{
		//fmt.Printf("list[%v]:%v,list[%v]:%v\n", i,j,list[i],list[j])
		//fmt.Println(nilNum, len(list))
		if list[i] == nil {
			nilNum++
			if list[j] == nil {
				nilNum++
				i++
				j--
				continue
			} else {
				return -1
			}
		}else  {
			if list[j] == nil {
				return -1
			}

			if list[i].Val != list[j].Val {
				return -1
			}
		}
		i++
		j--
	}

	if nilNum >= len(list) {
		return 1
	}
	return 0
}

func levelOrder(root *TreeNode) [][]int {
	result := [][]int{}

	nodeList := []*TreeNode{root}

	for len(nodeList) != 0 {
		tmpNodeList := []*TreeNode{}
		numList := []int{}
		for _,node := range nodeList {
			numList = append(numList, node.Val)
			if node.Left != nil {
				tmpNodeList = append(tmpNodeList, node.Left)
			}

			if node.Right != nil {
				tmpNodeList = append(tmpNodeList,node.Right)
			}
		}
		nodeList = tmpNodeList
		result = append(result,numList)
	}


	return result
}

func sortedArrayToBST(nums []int) *TreeNode {
	lenght := len(nums)
    if lenght == 0 {
		return nil
	}

	var node TreeNode
	if lenght == 1 {
		node.Val = nums[0]
		return &node
	}

	index := lenght/2
	node.Val = nums[index]
	node.Left = sortedArrayToBST(nums[:index])
	if index < lenght -1 {
		node.Right = sortedArrayToBST(nums[index+1:])
	}

	return &node

}


func main() {

	var node1 TreeNode;
	var node2 TreeNode;
	var node3 TreeNode;
	var node4 TreeNode;
	var node5 TreeNode;
	var node6 TreeNode;
	var node7 TreeNode;
	var node8 TreeNode;
	node1.Val = 1;
	node2.Val = 2;
	node3.Val = 3;
	node4.Val = 3;
	node5.Val = 4;
	node6.Val = 4;
	node7.Val = 3;
	node8.Val = 0;
	node1.Left = &node2
	node1.Right = &node3
	//node2.Left = &node4
	//node2.Right = &node5
	//node3.Left = &node6
	//node3.Right = &node7

	//res := isValidBST(&node1)
	//fmt.Println(res)

	res := isSymmetric(&node1)
	fmt.Println(res)
}