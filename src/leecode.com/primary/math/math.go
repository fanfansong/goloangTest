package main

import (
	"math"
	"strconv"
	"fmt"
)

func fizzBuzz(n int) []string {
	res := make([]string, n,n)
    for i := 1; i <= n; i++ {
		var tmp string
		if i % 3 == 0 {
			tmp += "Fizz"
		}
		if i %5 == 0 {
			tmp += "Buzz"
		}

		if len(tmp) == 0 {
			tmp = strconv.Itoa(i)
		}
		res[i-1] = tmp
	}
	return res
}

//如果数字过大速度太慢后面想一下别的方法
func countPrimes(n int) int {
	if n == 1 || n == 0{
		return 0
	}

	if n == 2 {
		return 1
	}

	res := []int{2}
	for i := 3; i <= n; i++ {
		flag := false
		for _,val := range res {
			if val*val > i {
				break
			}
			if i % val == 0 {
				flag = true
				break
			}
		}
		if !flag {
			fmt.Printf("res len:%v, i:%v\n" ,len(res),i)
			res = append(res, i)
		}
	}
	return len(res)	
}

//这个算法是去掉所有非质数
func countPrimes2(n int) int {
	if n < 3 {
		return 0
	}
	f := make([]bool, n)
	count := n / 2
	for i := 3; i*i < n; i += 2 {
		if f[i] {
			continue
		}
		for j := i * i; j < n; j += 2 * i {
			fmt.Printf("i:%v,j:%v\n",i,j)
			if !f[j] {
				count--
				f[j] = true
			}
		}
	}
	return count
}

//3的幂的特点：如果一个整数N是3的幂，那么其所有约数都是3的幂。那么，换一个角度，如果n小于N且是N的约数，那么其一定是3的幂；
func isPowerOfThree(n int) bool {
    if n <= 0 {
		return false
	}

	var val float64
	val = math.Log(float64(math.MaxInt32))/math.Log(3)
	intVal := int(val)
	max := math.Pow(3, float64(intVal))
	intMax := int(max)
	if intMax % n == 0 {
		return true
	}
	return false
}


func main() {
	res := countPrimes2(100)
	fmt.Println("countPrimes:", res)
	//countPrimes2(100)
}