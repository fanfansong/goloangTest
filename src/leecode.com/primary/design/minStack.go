package design

import (
	"math"
)

type MinStack struct {
    stack []int
}


/** initialize your data structure here. */
func Constructor() MinStack {
	var s MinStack
	s.stack = []int{}
	return s
}


func (this *MinStack) Push(x int)  {
    this.stack = append(this.stack,x)
}


func (this *MinStack) Pop()  {
    this.stack = this.stack[:len(this.stack) -1]
}


func (this *MinStack) Top() int {
    return this.stack[len(this.stack)-1]
}


func (this *MinStack) GetMin() int {
	min := math.MaxInt32
	length := len(this.stack)
	
	if length == 0 {
		return 0
	} 

	for _,val := range this.stack {
		if val < min {
			min = val
		}
	}
	return min
}
