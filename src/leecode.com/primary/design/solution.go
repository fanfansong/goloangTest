package design

type Solution struct {
	list []int
	m map[int]bool
}

func Constructor2(nums []int) Solution {
	var res Solution
	l := append([]int{},nums...)
	mm := make(map[int]bool) 
	for _,val := range nums {
		mm[val] = true
	}

	res.m = mm
	res.list = l
	return res
}


/** Resets the array to its original configuration and return it. */
func (this *Solution) Reset() []int {
    return this.list
}


/** Returns a random shuffling of the array. */
//不知道为什么结果没有通过，不知道是不是随机结果有问题
func (this *Solution) Shuffle() []int {
	length := len(this.list)
	res := make([]int, length,length)
	i := 0
	for key := range this.m {
		res[i] = key
		i++
	}
	return res
}