package main

import (
	"math"
	"fmt"
	"strings"
	"strconv"
)

func hammingDistance(x int, y int) int {
	tmp := x ^ y
	count := 0
	for i := 0; i <= 32; i++ {
		if tmp & (1 << uint(i)) != 0{
			count++
		}
	}
	return count
}

//这个利用字符串数数方式也挺有意思的，不过会慢一点
func hammingDistance2(x int, y int) int {
	return strings.Count(strconv.FormatInt(int64(x^y), 2), "1")
}

func reverseBits(n uint) uint {
	var res uint
	for i := 0; i < 32; i++ {
		tmp := (n >> uint(i)) & 1
		res = tmp << uint(31-i) | res
	}
	return res
}

//杨辉三角
func generate(numRows int) [][]int {
	res := [][]int{}
	if numRows == 0 {
		return res
	}

	res = append(res,[]int{1})
	if numRows == 1 {
		return res
	}

    for i := 2; i <= numRows; i++ {
		tmp := make([]int,i,i)
		tmp[0] = 1
		tmp[i-1] = 1
		if i - 2 <= 0 {
			res = append(res,tmp)
			continue
		}
		pre := res[i-1-1]
		for j := 1; j <= i -2; j++ {
			tmp[j] = pre[j-1] + pre[j]
		}
		res = append(res,tmp)
	}

	return res
}

func isValid(s string) bool {
	stack := []rune{}
	for _,val := range s {
		if val == '(' || val == '[' || val == '{' {
			stack = append(stack,val)
		}

		index := len(stack)-1
		if val == ')' || val == ']' || val == '}' { 
			if index < 0 {
				return false
			}
		}
		//fmt.Printf("val:%c,stack[%v]:%c\n",val,index,stack[index])
		if val == ')' {
			if stack[index] == '(' {
				stack = stack[:index]
			} else {
				return false
			}
		}

		if val == ']' {
			if stack[index] == '[' {
				stack = stack[:index]
			} else {
				return false
			}
		}

		if val == '}' {
			if stack[index] == '{' {
				stack = stack[:index]
			} else {
				return false
			}
		}

		fmt.Printf("len:%v\n",len(stack))
	}

	if len(stack) != 0 {
		return false
	}
	return true
}

func missingNumber(nums []int) int {
	max := math.MinInt32
	for _,val := range nums {
		if val > max {
			max = val
		}
	}

	list := make([]bool, max+1, max+1)
	for _,val := range nums {
		list[val] = true
	}

	for i,val := range list {
		if val != true {
			return i
		}
	}

	return 0
}

func main() {
	//res := hammingDistance(1,4)
	//fmt.Println("hammingDistance:", res)


	//var n uint = 43261596
	//res1 := reverseBits(n)
	//fmt.Println(strconv.FormatInt(int64(n),2))
	//fmt.Println(strconv.FormatInt(int64(res1),2))
	//fmt.Printf("reverseBits:%v\n", res1)

	//res2 := generate(5)
	//for _,list := range res2 {
	//	for _,val := range list {
	//		fmt.Printf("%v,",val)
	//	}
	//	fmt.Println("")
	//}

	res3 := isValid("]")
	fmt.Println("isValid:", res3)
	
}