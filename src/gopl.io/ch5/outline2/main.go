package main

import (
	"fmt"
	"net/http"
	"os"

	"golang.org/x/net/html"
)

func main() {
	resp, err := http.Get(os.Args[1])
    if err != nil {
        return
    }
    doc, err := html.Parse(resp.Body)
	resp.Body.Close()
	if err != nil {
        err = fmt.Errorf("parsing HTML: %s", err)
    return
	}
	
    //forEachNode(doc, startElement, endElement)
    forEachNode2(doc, ElementByID, nil);
}

// forEachNode针对每个结点x,都会调用pre(x)和post(x)。
// pre和post都是可选的。
// 遍历孩子结点之前,pre被调用
// 遍历孩子结点之后，post被调用
func forEachNode(n *html.Node, pre, post func(n *html.Node)) {
    if pre != nil {
        pre(n)
    }
    for c := n.FirstChild; c != nil; c = c.NextSibling {
        forEachNode(c, pre, post)
    }
    if post != nil {
        post(n)
    }
}

//练习 5.7： 完善startElement和endElement函数，使其成为通用的HTML输出器。要求：输出注释结点，
//文本结点以及每个元素的属性（< a href='...'>）。使用简略格式输出没有孩子结点的元素（即用<img/>代替<img></img>）。
//编写测试，验证程序输出的格式正确。（详见11章）
var depth int
func startElement(n *html.Node) {
    if n.Type == html.ElementNode {
        var attr string
        for _,a := range n.Attr {
            attr += " " + a.Key + "=" + "\"" + a.Val + "\" "
        }
        fmt.Printf("%*s<%s %s >\n", depth*2, "", n.Data, attr)
        depth++
    }
}
func endElement(n *html.Node) {
    if n.Type == html.ElementNode {
        depth--
        fmt.Printf("%*s</%s>\n", depth*2, "", n.Data)
    }
}

//练习 5.8： 修改pre和post函数，使其返回布尔类型的返回值。返回false时，中止forEachNoded的遍历。
//使用修改后的代码编写ElementByID函数，根据用户输入的id查找第一个拥有该id元素的HTML元素，查找成功后，停止遍历。
func forEachNode2(n *html.Node, pre, post func(n *html.Node, id string) bool) bool{
    id:="toc"
    if pre != nil {
        if flag:=pre(n, id);!flag {
            return flag
        }
    }
    for c := n.FirstChild; c != nil; c = c.NextSibling {
        if flag := forEachNode2(c, pre, post); !flag {
            return flag
        }
    }
    if post != nil {
        if flag:=post(n, id);!flag {
            return flag
        }
    }

    return true
}

func ElementByID(n *html.Node, id string) bool{
    if n.Type == html.ElementNode {
        for _,a := range n.Attr {
            if a.Key == "id" && a.Val == id {
                fmt.Printf("find id: %s\n", id)
                return false
            }
        }
    }
    return true
}